#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
ole.py
_________

For more information about ole file format,
visit http://www.reversenote.info/ole-file-format

"""

import mmap
import struct
from ctypes import *

__author__ = 'Youngbin Ha'
__copyrights__ = 'Copyright 2017 (C) WINS. ALL RIGHTS RESERVED'
__credits__ = []
__license__ = ''
__maintainer__ = 'Youngbin Ha'
__email__ = 'ybha@wins21.co.kr'
__status__ = 'developing'

__version__ = '0.1'
__date__ = '2017-10-10'
__updated__ = '2017-10-10'


_MAXFATENTRY = 109
_MAXREGSID = 0xFFFFFFFA
_NOSTREAM = 0xFFFFFFFF
_MAXREGSECT = 0xFFFFFFFA  # ÀÏ¹ÝÀûÀÎ ŒœÅÍÀÇ ³¡¹øÈ£ + 1
_ENDOFCHAIN = 0xFFFFFFFE  # ŒœÅÍ ÃŒÀÎÀÇ ³¡
_FREESECT = 0xFFFFFFFF  # ¹ÌÇÒŽçŒœÅÍ
_FATSECT = 0xFFFFFFFD  # FAT Sector
_DIFSECT = 0xFFFFFFFC  # DIFAT sector


class OleFile:
    """OLE file handling class"""

    class _CompoundFileHeader(LittleEndianStructure):
        """
        Compound File Header Structure

        """
        _MAXFATENTRY = 109

        _pack_ = 1
        _fields_ = [
            ('HeaderSignature', c_ubyte * 8),
            ('HeaderCLSID', c_ubyte * 16),
            ('MinorVersion', c_ushort),  # SHOULD be 0x003E if Major is 0x0003 or 0x0004
            ('MajorVersion', c_ushort),
            ('ByteOrder', c_ushort),  # MUST be set to 0xFFFE
            ('SectorShift', c_ushort),  # MUST be set to 0x0009, or 0x000c, depending of MajorVersion
            ('MiniSectorShift', c_ushort),
            ('Reserved', c_ubyte * 6),  # MUST 0
            ('NumberOfDirectorySectors', c_uint32),
            ('NumberOfFATSectors', c_uint32),
            ('FirstDirectorySectorLocation', c_uint32),
            ('TransactionSignatureNumber', c_uint32),
            ('MiniStreamCutoffSize', c_uint32),
            ('FirstMiniFATSectorLocation', c_uint32),
            ('NumberOfMiniFATSectors', c_uint32),
            ('FirstDIFATSectorLocation', c_uint32),
            ('NumberOfDIFATSectors', c_uint32),
            ('DIFAT', c_uint32 * _MAXFATENTRY)
        ]

        def make(self, buf):
            fit = min(len(buf), sizeof(self))
            memmove(addressof(self), buf, fit)

    class _DirectoryEntry(LittleEndianStructure):
        """
        Directory Entry Structure

        | offset | Field Name                  | Example                                               |
        |--------|-----------------------------|-------------------------------------------------------|
        | 0x0400 | Directory Entry Name        | Root Entry (section 2.6.2)                            |
        | 0x0440 | Directory Entry Name Length | 0x16 (22 bytes)                                       |
        | 0x0442 | Object Type                 | 0x05 (root storage)                                   |
        | 0x0443 | Color Flag                  | 0x01 (black)                                          |
        | 0x0444 | Left Sibling ID             | 0xFFFFFFFF (none)                                     |
        | 0x0448 | Right Sibling ID            | 0xFFFFFFFF (none)                                     |
        | 0x044C | Child ID                    | 0x00000001 (Stream ID 1: "Storage 1" (section 2.6.3)) |
        | 0x0450 | CLSID                       | 0x11CEC15456616700 0xAA005385 0x5BF9A100              |
        | 0x0460 | State Flags                 | 0x00000000                                            |
        | 0x0464 | Creation Time               | 0x0000000000000000                                    |
        | 0x046C | Modification Time           | 0x0000000000000000                                    |
        | 0x0474 | Starting Sector Location    | 0x00000003 (sector #3 for mini Stream)                |
        | 0x0478 | Stream Size                 | 0x0000000000000240 (576 bytes)                        |
        """
        _pack_ = 1
        _fields_ = [
            ('DirectoryEntryName', c_ubyte * 0x40),
            ('DirectoryEntryNameLength', c_short),
            ('ObjectType', c_ubyte),
            ('ColorFlag', c_ubyte),
            ('LeftSiblingID', c_uint32),
            ('RightSiblingID', c_uint32),
            ('ChildID', c_uint32),
            ('CLSID', c_ubyte * 0x10),
            ('StateFlags', c_uint32),
            ('CreationTime', c_uint64),
            ('ModificationTime', c_uint64),
            ('StartingSectorLocation', c_uint32),
            ('StreamSize', c_uint64)
        ]

        def make(self, buf):
            fit = min(len(buf), sizeof(self))
            memmove(addressof(self), buf, fit)

    def __init__(self, filename):
        """
        Initialize essential OLE file components

        to access stream and storage, we need a *map* calling *FAT Array* and *miniFAT Array*
        without map, we could not know where storage/stream to be stored

        so, most of all, this map must be prepared

        FAT Array is scattered over file,
        miniFAT Array may be scattered in FAT or just stored in file just one sector.

        FAT stream represents file itself
        miniFAT stream is ROOT ENTRY's stream itself

        .. note:: getting whole structure to parse is far too slow

        :param filehandle:
        """
        self._fp = open(filename, 'rb')
        self._mm = mmap.mmap(self._fp.fileno(), 0, access=mmap.ACCESS_READ)
        self._cfh = self._CompoundFileHeader()
        self._des = list()  # DirectoryEntryS
        self._dens = dict()  # DirectoryEntryNameS (name: DirectoryEntryID)
        self._fat_array = list()
        self._mfat_array = list()
        self._mfat_stream = None  # miniFAT stream
        self._namelist = list()  # this contains all archived file name as list

        # TODO: add exception handlers refer olefile package's OleFileIO class, there are tons of good references

        # set CompoundFileHeader (self._cfh)
        _buf = self._read_sector(-1)
        self._cfh.make(_buf)

        # prepare FAT sectors to build FAT(self._fat_array)
        _fat_sectors = list()
        for _i in range(0, 109):  # 109 = MAXFATENTRY
            if self._cfh.DIFAT[_i] == 0xFFFFFFFF or _i > self._cfh.NumberOfFATSectors:
                break
            _fat_sectors.append(self._cfh.DIFAT[_i])

        # prepare DIFAT sectors to build FAT(self._fat_array)
        _curr_difat_sector = self._cfh.FirstDIFATSectorLocation
        for _i in range(0, self._cfh.NumberOfDIFATSectors):  # only if DIFAT exists
            _buf = self._read_sector(_curr_difat_sector)
            _difat = self._littleEndianBinaryToList(_buf, dataSize=4)
            _curr_difat_sector = _difat[-1]  # last chain describes next sector
            _fat_sectors += _difat[:-1]  # gather DIFAT array except last chain

        # build FAT array(self._fat_array) from FAT/DIFAT sectors
        _buf = b''
        for _i in _fat_sectors:
            _buf += self._read_sector(_i)
        self._fat_array = self._littleEndianBinaryToList(_buf, dataSize=4)

        # build miniFAT(self._mfat_array)
        _start_sector = self._cfh.FirstMiniFATSectorLocation
        if _start_sector == 0xFFFFFFFE:
            # If all of the user streams in the file are greater than cutoff of 4096 bytes
            pass
        if self._cfh.NumberOfMiniFATSectors > 1:  # if this value is bigger than 1, refer FAT
            _buf = self._readStreamFromFATarray(_start_sector)
        else:  # or, just read specific sector from file
            _buf = self._read_sector(_start_sector)
        self._mfat_array = self._littleEndianBinaryToList(_buf)

    def namelist(self):
        """
        Display every DirectoryEntry's name

        name is limited to 32 Unicode UTF-16 code points

        `[MS-CFB] 2.6.1 Compound File Directory Entry <https://msdn.microsoft.com/en-us/library/dd942175.aspx>`_

        :return: archived name list
        :rtype: list
        """
        # if DirectoryEntry is not set yet, set it
        if not len(self._des):

            # get all DirectoryEntry data in ole
            _buf = self._readStreamFromFATarray(self._cfh.FirstDirectorySectorLocation)

            # convert _buf to _des(DirectoryEntryS) structure
            for i in range(0, len(_buf), 0x80):
                _de = self._DirectoryEntry()
                _de.make(_buf[i:i+0x80])

                # store DirectoryEntry to access with index(ChildID, Left/Right SiblingID)
                self._des.append(_de)

        return self._getDirectoryEntryNames()

    def read(self, name):
        """
        Read and return desired archived content

        :param name:
        :return: content
        :rtype: str
        """
        _buf = None

        try:
            _deid = self._dens[name]
            _buf = self._readDirectoryEntryStream(_deid)
        except KeyError:
            import adlog
            adlog.getlogger().error('no such stream name \'%s\'' % name)
            return _buf
        except Exception as e:
            import adlog
            adlog.getlogger().error('unexpected error while reading stream \'%s\'' % name)
            adlog.getlogger().error('error msg \'%s\'' % str(e))

        return _buf

    def _getDirectoryEntryNames(self, directoryEntryId=0, depth=''):
        _names = list()

        # get names from each DirectoryEntry
        _de = self._des[directoryEntryId]
        # DirectoryEntryName is ubyte ARRAY (DON'T FORGET!!!)
        _raw_name = ''.join(chr(_c) for _c in _de.DirectoryEntryName)
        # _name is still raw bytes, decode with utf16 and remove remaining null padding
        _name = _raw_name.decode('utf16').strip('\x00')

        # first, if this DirectoryEntry is STREAM, add it to namelist
        if _de.ObjectType == 0x02:  # 0x02 = STREAM
            _full_name = depth + _name
            _names.append(_full_name)
            # and store DirectoryEntryNameS to avoid further enc/decoding
            self._dens[_full_name] = directoryEntryId

        # in case of DirectoryEntry continues
        if _de.LeftSiblingID != 0xFFFFFFFF:  # 0xFFFFFFFF = NOSTREAM
            _names.extend(self._getDirectoryEntryNames(_de.LeftSiblingID, depth))
        if _de.RightSiblingID != 0xFFFFFFFF:
            _names.extend(self._getDirectoryEntryNames(_de.RightSiblingID, depth))
        if _de.ChildID != 0xFFFFFFFF:
            depth += _name + '/'
            _names.extend(self._getDirectoryEntryNames(_de.ChildID, depth))

        # no more DirectoryEntries
        return _names

    def _readDirectoryEntryStream(self, directoryEntryId):
        """
        Read desired stream's data

        :param directoryEntryId:
        :return: stream data
        """
        buf = b''
        _de = self._des[directoryEntryId]

        _stream_size = _de.StreamSize
        _start_sector = _de.StartingSectorLocation

        if _stream_size < self._cfh.MiniStreamCutoffSize and _de.ObjectType != 0x05:  # 0x05 = ROOT
            buf = self._readStreamFromMiniFATarray(_start_sector)
        else:
            buf = self._readStreamFromFATarray(_start_sector)

        return buf

    def _readStreamFromMiniFATarray(self, sector):
        """
        Gather scattered miniFAT stream to one

        follow miniFAT array chain
        :param sector:
        :return:
        """
        buf = b''
        _next_sector = sector
        # 0xFFFFFFFE EndOfChain, 0xFFFFFFFF NoStream, 0xFFFFFFFA MAXREGSID
        while _next_sector not in (0xFFFFFFFE, 0xFFFFFFFF) and _next_sector < 0xFFFFFFFA:
            buf += self._read_mini_sector(_next_sector)
            _next_sector = self._mfat_array[_next_sector]
        return buf

    def _readStreamFromFATarray(self, startSector):
        """
        Gather scattered stream pieces to one

        :param startSector: desired stream's start sector number
        :return: b''
        """
        buf = b''
        sectorNum = startSector
        while sectorNum not in (0xFFFFFFFE, 0xFFFFFFFF):  # 0xFFFFFFFE EndOfChain, 0xFFFFFFFF FreeSect
            buf = buf + self._read_sector(sectorNum)
            sectorNum = self._fat_array[sectorNum]
        return buf

    def _littleEndianBinaryToList(self, binaryData, dataSize=4):
        """
        Read LittleEndian data by specified dataSize

        :param binaryData: data to read
        :param dataSize: cutting size
        :return: list()
        """
        if len(binaryData) % dataSize != 0:
            return None
        return [struct.unpack('<I', binaryData[i:i + 4])[0] for i in range(0, len(binaryData), dataSize)]

    def _read_sector(self, sector):
        """
        Read file by specified sector number

        Note that CompoundFileHeader ALWAYS starts with **-1** index
        :param sector: sector number
        :return: read binary
        """
        _offset = (sector + 1) * 0x200  # 0x200 = sector size
        return self._mm[_offset:_offset+0x200]

    def _read_mini_sector(self, sector):
        """
        Read miniSector from miniFAT stream

        unlike _read_sector(), miniSector stream doesn't contain header
        so calculating offset formula is slightly different

        :param sector:
        :return:
        """

        if not self._mfat_stream:
            # TODO: redundant read() may occur, need to improve
            self._mfat_stream = self._readStreamFromFATarray(self._des[0].StartingSectorLocation)

        _offset = sector * 0x40  # MINISECTORSIZE
        return self._mfat_stream[_offset:_offset+0x40]


class ADMain:

    def __init__(self):
        self.handle = dict()
        pass

    def __del__(self):
        pass

    def format(self, filehandle, filename):
        """
        Specify file format

        :param filehandle: mmap handle
        :param filename:
        :return: dict, for further usage
        """
        _mm = filehandle  # mmap file handle
        _ret = {}

        if _mm[:8] == '\xD0\xCF\x11\xE0\xA1\xB1\x1A\xE1':  # DOCFILE0 A1B11AE1
            _ffi = 'OLE'  # additional File Format Information
            _ret = {'ff_ole': _ffi}  # return type must be dict()

        return _ret

    def arclist(self, filename, fileformat):
        """
        List archived child files

        return type MUST be (__name__, archived_files) pair

        :param filename:
        :param fileformat:
        :return:
        """
        child_files = []

        if 'ff_ole' in fileformat:
            olefile = self.__get_handle(filename)
            child_files.extend(olefile.namelist())

        return __name__, child_files

    def unarc(self, arc_engine_id, archived_file, archive_file):
        """
        Unarchive file binary from archive

        :param arc_engine_id: handler engine ID
        :type arc_engine_id: str
        :param archived_file: filename which archived
        :type archived_file: str
        :param archive_file: filename which wrote in file system
        :type archive_file: str
        :return: unarchived binary
        :rtype: str
        """
        if arc_engine_id == __name__:
            olefile = self.__get_handle(archive_file)
            try:
                return olefile.read(archived_file)
            except:
                pass

        return None

    def __get_handle(self, filename):
        """
        Retrieve pre defined handler or make one

        for memory efficiency, recommend to minimize file/class I/O
        to achieve this, every archive related plugins MUST support this method
        this method can reduce unnecessary I/O even unconsciously coding
        :param filename:
        :return:
        """
        if filename in self.handle:
            handle = self.handle.get(filename, None)
        else:
            handle = OleFile(filename)
            self.handle[filename] = handle

        return handle
